#ifndef ACCELEROMETER_DATA_H
#define ACCELEROMETER_DATA_H

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "timefunctionplot.h"

int acc_data_file;

void play_stop (GtkWidget *, TimeFunctionPlot *);

#endif /* include guard ACCELEROMETER_DATA_H */
