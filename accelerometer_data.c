#include <stdlib.h>
#include <stdio.h>
#include <gtk/gtk.h>
#include <fcntl.h>
#include <unistd.h>
#include <limits.h>
#include "accelerometer_data.h"
#include "timefunctionplot.h"

float accelero_data(){
	float tmp;
	float float_data;

	//precita udaj so suboru
	read (acc_data_file, &tmp, sizeof(float));
	float_data = (float) tmp / 1000;

	return float_data;
}

float random_data(){
	int tmp;
	float float_data;

	//precita udaj so suboru
	read (acc_data_file, &tmp, sizeof(int));
	float_data = (float)tmp / (float) INT_MAX;

	return float_data;
}

void data_shift (TimeFunctionPlot *tfp){
	//posunu sa data
	int i;

	for (i = tfp->data->rear; i < 500; i++){
		tfp->data->data[i] = tfp->data->data[i + 1];
	}
}

gboolean data_loading (TimeFunctionPlot *tfp){
	//nacita sa jedna hodnota a prekresli sa secko
	
	//zisti ci je urandom
	if (tfp->path[0] == '/' &&
		tfp->path[1] == 'd' &&
		tfp->path[2] == 'e' &&
		tfp->path[3] == 'v' &&
		tfp->path[4] == '/' &&
		tfp->path[5] == 'u' &&
		tfp->path[6] == 'r' &&
		tfp->path[7] == 'a' &&
		tfp->path[8] == 'n' &&
		tfp->path[9] == 'd' &&
		tfp->path[10] == 'o' &&
		tfp->path[11] == 'm'   ){
	
		tfp->data->data[500] = random_data();
	}
	else{

		tfp->data->data[500] = accelero_data();
	}

	gtk_widget_queue_draw (GTK_WIDGET(tfp->envelope));
	
	//ak pocitadlo klasne pod 0, prestane klesat
	if (tfp->data->rear != 0){
		tfp->data->rear = (tfp->data->rear - 1);
	}

	data_shift(tfp);

	return tfp->continue_play;
}

void start_timer (TimeFunctionPlot *tfp){
	//zacne pocitat cas 
	
	if (!tfp->play){
		g_timeout_add (100, data_loading, tfp);
		tfp->play = TRUE;
		tfp->continue_play = TRUE;
	}
}

void stop_timer (TimeFunctionPlot *tfp){
	tfp->continue_play = FALSE;
	tfp->play = FALSE;
}

void play_stop (GtkWidget *widget, TimeFunctionPlot *tfp){
	//nastavy premennu na ture alebo false a to zastavi alebo rozpohne program
	int i;

	g_print ("Plat_stop \n");
	g_print ("Tfp->path[5] a [11]: %c %c \n", tfp->path[5], tfp->path[11]);

	if (tfp->play == TRUE){
		stop_timer (tfp);

		//zavrie subor
		close (acc_data_file);

		tfp->play = FALSE;
	}
	else{
		acc_data_file = open (tfp->path, O_RDONLY);
	
		//osetrena chyba otvorenia suboru a koniec programu
		if (acc_data_file == -1){
			g_print ("Chyba v otvoreni suboru. \n");
			g_print ("Data sa nenacitali, koniec programu. \n");

			exit (EXIT_FAILURE);
		}

		//vynutenie neblokujuceho citania suboru
		int flags = fcntl(acc_data_file, F_GETFL, 0);
		if (fcntl (acc_data_file, F_SETFL, flags | O_NONBLOCK)){
			g_print ("Chyba pri vynucovani neblokujuceho citania suboru. \n");
		}

		start_timer (tfp);

		tfp->play = TRUE;
	}
}
