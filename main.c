#include <gtk/gtk.h>
#include <cairo.h>
#include "timefunctionplot.h"
#include "accelerometer_data.h"
#include "select_source_window.h"
#include "moving_with_mouse.h"
#include "size_of_window.h"

int main (int argc, char **argv){
	GtkWidget *window;
    TimeFunctionPlot *plotarea;
	GtkWidget *play_stop_button;
	GtkWidget *grid;
	CircQueue *circ_acc_data;
	int i;
	GtkWidget *menubar;
	GtkWidget *option;
	GtkWidget *option_mi;

	gtk_init (&argc, &argv);

    if( (circ_acc_data = malloc(sizeof(CircQueue))) == NULL ){
		return 0;
	}

	//nastavia sa data na 0 pre zaciatok a ukazovatel sa nastavi na zaciatok
	circ_acc_data->rear = 500;
		
	for (i = 0; i < 500; i++){
		circ_acc_data->data[i] = 0;
	}

	//vytvorenie okna a jeho nastavenia
	window=gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_window_set_position (GTK_WINDOW (window), GTK_WIN_POS_CENTER);
	gtk_window_set_default_size (GTK_WINDOW (window), 600, 300);
	g_signal_connect (window, "delete-event", G_CALLBACK (gtk_main_quit), NULL);

	//vytvorenie mriezky
	grid = gtk_grid_new ();
	gtk_container_add (GTK_CONTAINER (window), grid);

	//vytvorenie plotarei
    plotarea = tfp_new (circ_acc_data);
	gtk_grid_attach (GTK_GRID (grid), tfp_get_enveloping_widget (plotarea), 0, 1, 1, 1);

	//menubar, v ktorom sa bude vyberat zdroj dat
	menubar = gtk_menu_bar_new ();
	gtk_widget_set_hexpand (menubar, TRUE);

	//zavola sa funkcia na vyrabanie poloziek v menubar
	option = select_source (plotarea);
	
	option_mi = gtk_menu_item_new_with_label ("Option");
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (option_mi), option);
	gtk_menu_shell_append (GTK_MENU_SHELL (menubar), option_mi);
	gtk_grid_attach (GTK_GRID (grid), menubar, 0, 0, 1, 1);

	//odchitinie velkosti okna
	g_signal_connect (G_OBJECT (window), "configure-event", G_CALLBACK (get_size), plotarea);

	//nacitanie dat s akcelerometra
	play_stop_button = gtk_button_new_with_label ("Play / stop");
	g_signal_connect (G_OBJECT (play_stop_button), "clicked", G_CALLBACK (play_stop), plotarea);
	gtk_grid_attach (GTK_GRID (grid), play_stop_button, 0, 2, 1, 1);

	//posuvanie sa po drawing arei
	gtk_widget_set_events (tfp_get_enveloping_widget (plotarea), gtk_widget_get_events (tfp_get_enveloping_widget (plotarea))
			               | GDK_LEAVE_NOTIFY_MASK
						   | GDK_BUTTON_PRESS_MASK
						   | GDK_POINTER_MOTION_MASK
						   | GDK_POINTER_MOTION_HINT_MASK);

	g_signal_connect (tfp_get_enveloping_widget (plotarea), "motion-notify-event", G_CALLBACK (motion_event), plotarea);
	g_signal_connect (tfp_get_enveloping_widget (plotarea), "button-press-event", G_CALLBACK (on_button_press), plotarea);

	gtk_widget_show_all (window);

	gtk_main ();

	return 0;
}
