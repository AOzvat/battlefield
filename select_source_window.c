#include <gtk/gtk.h>
#include <dirent.h>
#include <stdlib.h>
#include "timefunctionplot.h"

int dir_content (char *dir_cont[20]){
	DIR *d = opendir ("/dev");
	struct dirent *dir;
	char *name;
	char tty[6] = {'t','t','y','A','C','M'};
	int i = 0;

	//ak sa nepodari otvorit adresar
	if (d == NULL){
		g_print ("Nepodarilo sa otvorit adresar /dev. \n");

		return 0;
	}

	//do dir_cont sa vlozia iba veci s ttyACMnieco
	while ((dir = readdir(d)) != NULL){
		name = dir->d_name;

		if (tty[0] == name[0] &&
			tty[1] == name[1] &&
			tty[2] == name[2] &&
			tty[3] == name[3] &&
			tty[4] == name[4] &&
			tty[5] == name[5]   ){

			dir_cont[i] = dir->d_name;

			i++;
		}
	}

	closedir (d);

	return i;
}

void path_to_the_file (GtkWidget *menu_item, TimeFunctionPlot *tfp){
	const char *menu_item_label;

	//vytvorenie miesta pre text s labelu v radio_buttone
    if( (menu_item_label = malloc(7 * sizeof(char))) == NULL){
		return;
	}

	g_print ("Path to the file \n");

	//zapamata si nazov s oznaceneho radio_buttonu
	menu_item_label = gtk_menu_item_get_label (menu_item);


	g_print ("Menu_item_label[0] a [6]: %c %c \n", menu_item_label[0], menu_item_label[6]);

	//nastavenie cesty
	tfp->path[0] = '/';
	tfp->path[1] = 'd';
 	tfp->path[2] = 'e';
 	tfp->path[3] = 'v';
 	tfp->path[4] = '/';
 	tfp->path[5] = menu_item_label[0];
 	tfp->path[6] = menu_item_label[1];
 	tfp->path[7] = menu_item_label[2];
 	tfp->path[8] = menu_item_label[3];
 	tfp->path[9] = menu_item_label[4];
 	tfp->path[10] = menu_item_label[5];
 	tfp->path[11] = menu_item_label[6];

	g_print ("Tfp->path[1]: %c ", tfp->path[1]);
	g_print ("Tfp->path[11]: %c \n", tfp->path[11]);
}

GtkWidget *select_source (TimeFunctionPlot *tfp){
	GtkWidget *menu, *menu_item;
	GtkRadioMenuItem *item;
	int i;
	char *dir_cont[20];
	int count_source = 0;

	//zistenie stav adresara a ich pocet
	count_source = dir_content (dir_cont);

	menu = gtk_menu_new ();
	item = NULL;
	
	//prida tolko radiobuttonov kolko je ttyACMnieco v /dev
	for (i = 0; i < count_source + 1; i++){
		if (i == 0){
			//prida iba random
			menu_item = gtk_radio_menu_item_new_with_label_from_widget (NULL, "urandom");
			gtk_radio_menu_item_join_group (GTK_RADIO_MENU_ITEM (menu_item), item);
			item = GTK_RADIO_MENU_ITEM (menu_item);
			gtk_menu_shell_append (GTK_MENU_SHELL (menu), menu_item);
		}
		else{
			menu_item = gtk_radio_menu_item_new_with_label_from_widget (NULL, dir_cont[i - 1]);
			gtk_radio_menu_item_join_group (GTK_RADIO_MENU_ITEM (menu_item), item);
			item = GTK_RADIO_MENU_ITEM (menu_item);
			gtk_menu_shell_append (GTK_MENU_SHELL (menu), menu_item);
		}

		//nastavi co sa ma stat ked sa oznaci jedna s poloziek menubaru
		g_signal_connect (G_OBJECT (menu_item), "activate", G_CALLBACK (path_to_the_file), tfp);
	}

	return menu;
}

