#include <gtk/gtk.h>
#include "moving_with_mouse.h"

void draw_brush (GtkWidget *widget, gdouble x,  gdouble y, TimeFunctionPlot *tfp){
	tfp->zero_x = tfp->zero_x - x;
	tfp->zero_y = tfp->zero_y - y;

	gtk_widget_queue_draw (GTK_WIDGET(tfp->envelope));
}

gboolean on_button_press (GtkWidget *widget, GdkEventButton *event, TimeFunctionPlot *tfp){
	//odchytenie pozicii mysu pri stlaceni
	if (event->button == GDK_BUTTON_PRIMARY){
		tfp->last_event_x = event->x;
		tfp->last_event_y = event->y;
	}

	return TRUE;
}

gboolean motion_event (GtkWidget *widget, GdkEventMotion *event, TimeFunctionPlot *tfp){
	int x, y;
    int delta_x, delta_y;
	GdkModifierType state;
	
	gdk_window_get_device_position (event->window, event->device, &x, &y, &state);

	//odchytenie a prepocet pozicii ked sa myska pohybuje
    delta_x = tfp->last_event_x - x;
    delta_y = tfp->last_event_y - y;

    tfp->last_event_x = x;
    tfp->last_event_y = y;

	if (state & GDK_BUTTON1_MASK){
		draw_brush (widget, delta_x, delta_y, tfp);
	}

	return TRUE;
}
