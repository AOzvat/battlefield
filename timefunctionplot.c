#include <gtk/gtk.h>
#include <stdlib.h>
#include <math.h>
#include "timefunctionplot.h"
#include "accelerometer_data.h"

void axes (cairo_t *c, TimeFunctionPlot *tfp_data){
	cairo_set_source_rgb (c,0.5,0.5,0.5);

	//x-os
	cairo_move_to (c, -tfp_data->zero_x + tfp_data->zero_x, 0 + tfp_data->zero_y);
	cairo_line_to (c, 3000 + tfp_data->zero_x, 0 + tfp_data->zero_y);

	//y-os
	cairo_move_to (c, 0 + tfp_data->zero_x, -tfp_data->zero_y + tfp_data->zero_y);
	cairo_line_to (c, 0 + tfp_data->zero_x, 3000 + tfp_data->zero_y);

	//dolna ciara
	cairo_move_to (c, -tfp_data->zero_x + tfp_data->zero_x, tfp_data->zero_y - 50 + tfp_data->height);
	cairo_line_to (c, 3000 + tfp_data->zero_x, tfp_data->zero_y - 50 + tfp_data->height);

	cairo_stroke (c);
	cairo_set_source_rgb (c,0,0,0);
}

void limitation (cairo_t *c, TimeFunctionPlot *tfp_data){
	int i;

	cairo_set_source_rgb (c,0.8,0.8,0.8);

	//y-os
	//limita na +-1 az +-10
	for (i = 1; i <= 10; i++){	
		cairo_move_to (c, -300 * tfp_data->scale_x + tfp_data->zero_x, -i * tfp_data->scale_y + tfp_data->zero_y);
		cairo_line_to (c, 300 * tfp_data->scale_x + tfp_data->zero_x, -i * tfp_data->scale_y + tfp_data->zero_y);

		cairo_move_to (c, -300 * tfp_data->scale_x + tfp_data->zero_x, i * tfp_data->scale_y + tfp_data->zero_y);
		cairo_line_to (c, 300 * tfp_data->scale_x + tfp_data->zero_x, i * tfp_data->scale_y + tfp_data->zero_y);
	}

	//limita na +-0.5 (objavuje sa po vacsom priblizeni)
	if (tfp_data->scale_y>=110){
		cairo_move_to (c, -300 * tfp_data->scale_x + tfp_data->zero_x, -0.5 * tfp_data->scale_y + tfp_data->zero_y);
		cairo_line_to (c, 300 * tfp_data->scale_x + tfp_data->zero_x, -0.5 * tfp_data->scale_y + tfp_data->zero_y);

		cairo_move_to (c, -300 * tfp_data->scale_x + tfp_data->zero_x, 0.5 * tfp_data->scale_y + tfp_data->zero_y);
		cairo_line_to (c, 300 * tfp_data->scale_x + tfp_data->zero_x, 0.5 * tfp_data->scale_y + tfp_data->zero_y);
	}

	//x-os
	//limita na 1-10
	for (i = 1; i <= 10; i++){
	cairo_move_to (c, i * tfp_data->scale_x + tfp_data->zero_x, -600 * tfp_data->scale_y + tfp_data->zero_y);
	cairo_line_to (c, i * tfp_data->scale_x + tfp_data->zero_x, 600 * tfp_data->scale_y + tfp_data->zero_y);
	}

	cairo_stroke (c);
	cairo_set_source_rgb (c,0,0,0);
}

void graph_line (cairo_t *c, TimeFunctionPlot *tfp_data){
	int i;
	float x[500], y[500];

	//naplnia sa polia
	for (i=0; i<499; i++){
		x[i] = 0.02 *i;
		y[i] = tfp_data->data->data[i];
	}

	cairo_set_source_rgb (c,1,0,0);

	//vykreslia sa polia
	for (i=0;i<498;i++){
		cairo_move_to (c, (x[i]) * tfp_data->scale_x + tfp_data->zero_x, (0-(y[i])) * tfp_data->scale_y + tfp_data->zero_y);
		cairo_line_to (c, (x[i+1]) * tfp_data->scale_x + tfp_data->zero_x, (0-(y[i+1]))* tfp_data->scale_y + tfp_data->zero_y);
	}

	cairo_stroke (c);
	cairo_set_source_rgb (c,0,0,0);
}

void text (cairo_t *c, TimeFunctionPlot *tfp_data){
	int i;
	static const char* const character_positive[] = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};
	static const char* const character_negative[] = {"-1", "-2", "-3", "-4", "-5", "-6", "-7", "-8", "-9", "-10"};

	cairo_select_font_face (c,"Sans",CAIRO_FONT_SLANT_NORMAL,CAIRO_FONT_WEIGHT_NORMAL);
	cairo_set_font_size (c,20);

	cairo_set_source_rgb (c,0,0,0);
	
	//y-os
	//zobrazi sa 0
	cairo_move_to (c, -13 + tfp_data->zero_x, 20 + tfp_data->zero_y);
	cairo_show_text (c, "0");

	//zobrazi sa +-1 az +-9
	for (i = 1; i < 10; i++){
		tfp_data->scale_y *= i;
		cairo_move_to (c, -13 + tfp_data->zero_x, -tfp_data->scale_y - 5 + tfp_data->zero_y);
		cairo_show_text (c, character_positive[i - 1]);
		cairo_move_to (c, -20 + tfp_data->zero_x, tfp_data->scale_y + 18 + tfp_data->zero_y);
		cairo_show_text (c, character_negative[i - 1]);
		tfp_data->scale_y /= i;
	}

	//je to mimo ciklu lebo 10 je dvojmiestne cislo!
	//zobrazi sa +-10
	tfp_data->scale_y *= 10;
	cairo_move_to (c, -26 + tfp_data->zero_x, -tfp_data->scale_y - 5 + tfp_data->zero_y);
	cairo_show_text (c, character_positive[9]);
	cairo_move_to (c, -33 + tfp_data->zero_x, tfp_data->scale_y + 18 + tfp_data->zero_y);
	cairo_show_text (c, character_negative[9]);
	tfp_data->scale_y /= 10;

	//zobrazi sa +-0.5
	if (tfp_data->scale_y > 110){
		tfp_data->scale_y *= 0.5;
		cairo_move_to (c, -31 + tfp_data->zero_x, -tfp_data->scale_y-5 + tfp_data->zero_y);
		cairo_show_text (c, "0.5");
		cairo_move_to (c, -38 + tfp_data->zero_x, tfp_data->scale_y+18 + tfp_data->zero_y);
		cairo_show_text (c, "-0.5");
		tfp_data->scale_y /= 0.5;
	}

	//x-os
	//zobrazi sa 1
	cairo_move_to (c, tfp_data->scale_x + 5 + tfp_data->zero_x, tfp_data->zero_y - 30 + tfp_data->height);
	cairo_show_text (c, "1");

	//zobrazi sa 2-10
	for (i = 2; i <= 10; i++ ){
		cairo_move_to (c, tfp_data->scale_x * i + 6 + tfp_data->zero_x, tfp_data->zero_y - 30 + tfp_data->height);
		cairo_show_text (c, character_positive[i - 1]);
	}

	cairo_stroke (c);
}

gboolean event_draw (GtkWidget *widget, cairo_t *c, TimeFunctionPlot *tfp_data)
{
	axes (c, tfp_data);

	limitation (c, tfp_data);

	graph_line (c, tfp_data);

	text (c, tfp_data);

    return FALSE;
}

gboolean event_scroll (GtkWidget *widget, GdkEvent *event, TimeFunctionPlot *tfp_data)
{
	GdkScrollDirection direction;

    gdk_event_get_scroll_direction (event,&direction);

	//zoom
	if (direction == GDK_SCROLL_UP){
		//po hodnote 110 sa musi pripocitovat 2, aby to fungovalo
		if (tfp_data->scale_y < 110){
			tfp_data->scale_x += 1;
			tfp_data->scale_y += 1;
		}
		else {
			tfp_data->scale_x += 2;
			tfp_data->scale_y += 2;
		}
	   	gtk_widget_queue_draw (GTK_WIDGET(widget));
	}

	//zoom out
	if (direction == GDK_SCROLL_DOWN){	
		//aby sa neprevratil graf
		if (tfp_data->scale_x > 0){
			tfp_data->scale_x -= 1;
			tfp_data->scale_y -= 1;
		}
		gtk_widget_queue_draw (GTK_WIDGET(widget));
	}

    return FALSE;
}

TimeFunctionPlot *tfp_new(CircQueue *circ_acc_data)
{
    GtkWidget        *surface;
    TimeFunctionPlot *to_create;

    if( (to_create = malloc(sizeof(TimeFunctionPlot))) == NULL )
        return NULL;

    if( (surface = gtk_drawing_area_new()) == NULL )
    {
        free(to_create);
        return NULL;
    }

	//vytvorenie miesta pre path
    if( (to_create->path = malloc(12 * sizeof(char))) == NULL){
		return NULL;
	}

	//platno dostane priestor navyse v gride
    gtk_widget_set_hexpand(surface, TRUE);
    gtk_widget_set_vexpand(surface, TRUE);

	//inicializacia
    to_create->envelope = surface;

	to_create->scale_x = 50;
	to_create->scale_y = 50;

	to_create->data = circ_acc_data;

	//nastavy ci sa citaju data so suboru alebo nie
	to_create->play = FALSE;
	to_create->continue_play = FALSE;

	//drawing
    g_signal_connect(G_OBJECT(surface), "draw", G_CALLBACK(event_draw), to_create);
	gtk_widget_queue_draw (GTK_WIDGET(surface));

	//scrolling
	gtk_widget_add_events (GTK_WIDGET(surface),GDK_SCROLL_MASK);
	g_signal_connect (G_OBJECT(surface),"scroll-event",G_CALLBACK(event_scroll), to_create);

    return to_create;
}


GtkWidget *tfp_get_enveloping_widget(TimeFunctionPlot *tfp_data)
{
    return tfp_data->envelope;
}
